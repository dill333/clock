function padNumber(number) {
  if (number < 10) {
    return `0${number}`;
  }
  return number.toString();
}

function getDayWord(day) {
  switch (day) {
    case 0:
      return "SUN";
    case 1:
      return "MON";
    case 2:
      return "TUE";
    case 3:
      return "WED";
    case 4:
      return "THU";
    case 5:
      return "FRI";
    case 6:
      return "SAT";
    default:
      return "UNK";
  }
}

function dateString(date) {
  const day = date.getDate();
  const dayWord = getDayWord(date.getDay());
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return `${dayWord} - ${padNumber(month)}/${padNumber(day)}/${year}`;
}

function timeString(date) {
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();

  return `${padNumber(hours)}:${padNumber(minutes)}:${padNumber(seconds)}`;
}

function updatePage() {
  const date = new Date();

  const timeStr = timeString(date);
  const dateStr = dateString(date);

  document.getElementById("clock").innerHTML = timeStr;
  document.getElementById("date").innerHTML = dateStr;
}

function createInterval() {
  window.setInterval(() => updatePage(), 500);
}
