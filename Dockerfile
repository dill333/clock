FROM nginx:alpine

WORKDIR /app

COPY index.html /var/www/public/index.html
COPY index.css /var/www/public/index.css
COPY index.js /var/www/public/index.js
COPY digital-7.ttf /var/www/public/digital-7.ttf
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

ENTRYPOINT ["nginx"]